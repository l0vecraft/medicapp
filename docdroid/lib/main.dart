import 'package:docdroid/core/controllers/auth/authController.dart';
import 'package:docdroid/routes/routes.dart';
import 'package:docdroid/ui/views/homeView.dart';
import 'package:docdroid/ui/widgets/notifications/push_notification.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await PushNotificationsManager().init();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData().copyWith(primaryColor: Color(0xff03ffbc)),
      title: 'Docdroid',
      home: HomeView(),
      getPages: MedicRoutes().pages,
      initialRoute: '/',
    );
  }
}
