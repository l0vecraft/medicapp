import 'package:docdroid/core/data/repository/doctor_repository.dart';
import 'package:docdroid/core/models/doctors.dart';
import 'package:get/get.dart';

class DoctorController extends GetxController {
  DoctorRepository _fakeData = DoctorRepository();

  RxList _doctorsList = [].obs;

  Rx<Doctors> _doctor = Doctors().obs;

  RxList get doctorsList => _doctorsList;
  Rx<Doctors> get doctor => _doctor;

  @override
  void onInit() {
    super.onInit();
    _getAllDoctors();
  }

  Future<void> _getAllDoctors() async {
    List<Doctors> res = await _fakeData.getAllDoctors();
    _doctorsList.addAll(res);
  }

  Future<void> getDoctor(int id) async {
    var res = await _fakeData.getDoctor(id);
    _doctor.update((val) {
      val.id = res.id;
      val.address = res.address;
      val.distance = res.distance;
      val.name = res.name;
      val.genero = res.genero;
      val.image = res.image;
      val.specialization = res.specialization;
      val.score = res.score;
      val.reviews = res.reviews;
    });
  }
}
