import 'package:docdroid/core/controllers/doctors/doctor.dart';
import 'package:docdroid/routes/all_routes.dart';

class DoctorBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DoctorController());
  }
}
