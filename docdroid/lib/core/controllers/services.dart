import 'package:docdroid/core/models/services.dart';
import 'package:get/get.dart';

class ServiceController extends GetxController {
  RxList<Services> _listServices = servicesList.obs;

  get listService => _listServices;
}
