import 'package:docdroid/core/data/repository/hospital_repository.dart';
import 'package:docdroid/core/models/hospitals.dart';
import 'package:get/get.dart';

class HospitalController extends GetxController {
  final HospitalRepository _hospitalRepository = HospitalRepository();

  RxList<Hospitals> _hospitalList = [].obs;
  Rx<Hospitals> _hospital = Hospitals().obs;

  get hospitalList => _hospitalList;
  Rx<Hospitals> get hospital => _hospital;

  @override
  void onInit() {
    super.onInit();
    getHospitals();
  }

  Future<void> getHospitals() async {
    List<Hospitals> res = await _hospitalRepository.getAllHospitals();
    _hospitalList.addAll(res);
  }

  Future<void> getHospital(int id) async {
    Hospitals res = await _hospitalRepository.getHospital(id);
    _hospital.update((val) {
      val.id = res.id;
      val.address = res.address;
      val.distance = res.distance;
      val.image = res.image;
      val.name = res.name;
      val.score = res.score;
      val.specialization = res.specialization;
    });
  }
}
