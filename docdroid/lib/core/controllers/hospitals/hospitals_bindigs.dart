import 'package:docdroid/core/controllers/hospitals/hospitals.dart';
import 'package:docdroid/routes/all_routes.dart';

class HospitalsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HospitalController>(() => HospitalController());
  }
}
