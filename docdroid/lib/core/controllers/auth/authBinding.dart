import 'package:docdroid/core/controllers/auth/authController.dart';
import 'package:docdroid/core/data/repository/api_repository.dart';
import 'package:docdroid/core/data/repository/local_storage_repository.dart';
import 'package:docdroid/routes/all_routes.dart';

class AuthBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AuthController(
        apiInterface: ApiRepository(),
        localStorageInterface: LocalStorageRepository()));
  }
}
