import 'package:docdroid/core/data/repository/api_interface.dart';
import 'package:docdroid/core/data/repository/api_repository.dart';
import 'package:docdroid/core/data/repository/local_storage_interface.dart';
import 'package:docdroid/core/data/repository/local_storage_repository.dart';
import 'package:docdroid/routes/all_routes.dart';

class AuthController extends GetxController {
  final LocalStorageInterface localStorageInterface;
  final ApiInterface apiInterface;
  final LocalStorageRepository local = LocalStorageRepository();
  final ApiRepository api = ApiRepository();

  RxString _token = ''.obs;
  RxBool _isFinish = false.obs;

  String get token => _token.value;
  bool get isFinish => _isFinish.value;

  set token(String newValue) => _token.value = newValue;
  AuthController({this.localStorageInterface, this.apiInterface});

  @override
  void onReady() {
    super.onReady();
    validateSession();
  }

  void validateSession() async {
    _token.value = await local.getToken();
    if (_token.value != null) {
      final user = await api.getUserFromToken(_token.value);
      await local.saveUser(user);
      Get.toNamed('/menu');
    } else {
      Get.toNamed('/login');
    }
  }

  void logOut() async {
    Future.delayed(Duration(seconds: 2));
    _token.value = null;
    await local.clearAllData();
    print('todo limpio');
  }
}
