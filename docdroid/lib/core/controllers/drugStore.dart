import 'package:docdroid/core/models/drugstore.dart';
import 'package:get/get.dart';

class DrugControlller extends GetxController {
  RxList<DrugStore> _storeList = drugstore.obs;

  get storeList => _storeList;
}
