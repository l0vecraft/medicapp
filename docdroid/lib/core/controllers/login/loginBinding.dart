import 'package:docdroid/core/controllers/login/loginController.dart';
import 'package:docdroid/core/data/repository/api_repository.dart';
import 'package:docdroid/core/data/repository/local_storage_repository.dart';
import 'package:docdroid/routes/all_routes.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LoginController(
        apiInterface: ApiRepository(),
        localStorageInterface: LocalStorageRepository()));
  }
}
