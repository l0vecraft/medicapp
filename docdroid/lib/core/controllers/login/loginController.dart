import 'package:docdroid/core/data/repository/api_interface.dart';
import 'package:docdroid/core/data/repository/local_storage_interface.dart';
import 'package:docdroid/core/models/login_request.dart';
import 'package:docdroid/core/models/user.dart';
import 'package:docdroid/routes/all_routes.dart';
import 'package:flutter/cupertino.dart';

class LoginController extends GetxController {
  final LocalStorageInterface localStorageInterface;
  final ApiInterface apiInterface;

  LoginController({this.localStorageInterface, this.apiInterface});

  //* estos elementos solo son para capturar las entradas en el login
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  Future<bool> login() async {
    final User user =
        User(name: usernameController.text, password: passwordController.text);
    try {
      LoginRequest response =
          await apiInterface.login(user); //* se llama el api

      await localStorageInterface.saveToken(
          response.token); //* una vez obtenido los datos se guarda el token

      await localStorageInterface
          .saveUser(response.user); //* y se guardan los datos del usuario

      return true; //* si todo salio bien se envia true
    } on NoSuchMethodError catch (e) {
      return false; //* de lo contrario false
    }
  }
}
