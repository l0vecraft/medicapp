import 'package:docdroid/core/controllers/citas/citas.dart';
import 'package:docdroid/routes/all_routes.dart';

class CitasBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CitasController());
  }
}
