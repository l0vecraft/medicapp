import 'package:docdroid/core/models/citas.dart';
import 'package:get/get.dart';

class CitasController extends GetxController {
  /*
  * bueno se supone que cada usuario, debe tener un ID unico
  * de esa forma un usuario puede pedir varias citas sin que se cruzen
  * entre si 

    * entonces la vaina seria asi:
    1. selecciono al doctor en agendar la cita
    2. capturo al doctor y se lo paso al widget calendario
    3. dejo presionado un dia para que me salga algo para cuadrar los servicios
    4. le doy aceptar
    5. creo un objeto cita para que se guarde con el metodo addCita
    6. llamo el listCitas
  * */
  RxList _citas = [].obs;
  get listCitas => _citas;

  //TODO: implementar toda la logica pero avanzada

  void addCita(Citas c) {
    _citas.add(c);
  }

  getCita(int id) {
    return _citas[id];
  }

  void cancelCita(int id) {
    _citas.removeAt(id);
  }
}
