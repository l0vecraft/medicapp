import 'package:get/get.dart';
import 'package:tflite/tflite.dart';

class DetectorController extends GetxController {
  String _model = 'assets/model.tflite';
  String _labels = 'assets/labels.txt';
  RxBool _loading = true.obs;
  RxList _output = [].obs;

  bool get loading => _loading.value;
  RxList get output => _output;

  set loading(newValue) => _loading.value = newValue;

  initModel() async {
    await Tflite.loadModel(model: _model, labels: _labels);
  }

  classifyImage(String path) async {
    var tmp = await Tflite.runModelOnImage(
        path: path,
        numResults: 3,
        threshold: 0.5,
        imageMean: 127.5,
        imageStd: 127.5);
    _loading.value = false;
    _output.addAll(tmp);
  }

  @override
  void onReady() {
    super.onReady();
    initModel();
  }

  @override
  onClose() {
    super.onClose();
    Tflite.close();
  }
}
