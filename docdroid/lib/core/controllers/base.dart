
import 'package:flutter/widgets.dart';

enum ViewState{
  Busy,Idel
}

class BaseController extends ChangeNotifier{
  ViewState _state = ViewState.Idel;

  get state=> _state;

  set setViewState(ViewState state){
    _state = state;
    notifyListeners();
  }
}