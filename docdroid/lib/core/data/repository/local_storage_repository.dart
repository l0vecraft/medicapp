import 'package:docdroid/core/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'local_storage_interface.dart';

class LocalStorageRepository implements LocalStorageInterface {
  @override
  Future<void> clearAllData() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.clear();
  }

  @override
  Future<String> getToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString('TOKEN');
  }

  @override
  Future<User> getUser() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    final username = sharedPreferences.getString('USERNAME');
    final age = sharedPreferences.getInt('AGE');
    final blood = sharedPreferences.getString('BLOOD');
    final gender = sharedPreferences.getString('GENDER');
    final height = sharedPreferences.getDouble('HEIGHT');
    final weight = sharedPreferences.getDouble('WEIGHT');
    final user = User(
        name: username,
        age: age,
        blood: blood,
        gender: gender,
        height: height,
        weight: weight);
    return user;
  }

  @override
  Future<User> saveUser(User user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString('USERNAME', user.name);
    sharedPreferences.setInt('AGE', user.age);
    sharedPreferences.setString('BLOOD', user.blood);
    sharedPreferences.setString('GENDER', user.gender);
    sharedPreferences.setDouble('HEIGHT', user.height);
    sharedPreferences.setDouble('WEIGHT', user.weight);
    return user;
  }

  @override
  Future saveToken(String token) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString('TOKEN', token);
    return token;
  }
}
