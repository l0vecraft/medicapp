import 'package:docdroid/core/models/doctors.dart';

abstract class DoctorInterface {
  Future<List<Doctors>> getAllDoctors();
  Future<Doctors> getDoctor(int index);
}
