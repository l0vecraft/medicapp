import 'package:docdroid/core/models/hospitals.dart';

abstract class HospitalInterface {
  Future<List<Hospitals>> getAllHospitals();
  Future<Hospitals> getHospital(int id);
}
