import 'package:docdroid/core/data/apis/mocks/fake_hospitals.dart';
import 'package:docdroid/core/data/repository/hospital_interface.dart';
import 'package:docdroid/core/models/hospitals.dart';

class HospitalRepository extends HospitalInterface {
  final FakeHospitals _fakeHospitals = FakeHospitals();
  @override
  Future<List<Hospitals>> getAllHospitals() async {
    List<Hospitals> result;
    try {
      result = await _fakeHospitals.getAllHospitals();
      return result;
    } catch (e) {
      throw e;
    }
  }

  @override
  Future<Hospitals> getHospital(int id) async {
    Hospitals result;
    try {
      result = await _fakeHospitals.getHospital(id);
      return result;
    } catch (e) {
      throw e;
    }
  }
}
