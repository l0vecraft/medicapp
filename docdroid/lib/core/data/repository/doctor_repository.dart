import 'package:docdroid/core/data/apis/mocks/fake_doctors.dart';
import 'package:docdroid/core/data/repository/doctor_interface.dart';
import 'package:docdroid/core/models/doctors.dart';

class DoctorRepository extends DoctorInterface {
  final FakeDoctorApi _fakeDoctorApi = FakeDoctorApi();
  @override
  Future<List<Doctors>> getAllDoctors() async {
    List<Doctors> result;
    try {
      result = await _fakeDoctorApi.getDoctors();
      return result;
    } catch (e) {
      throw e;
    }
  }

  @override
  Future<Doctors> getDoctor(int index) async {
    Doctors result;
    try {
      result = await _fakeDoctorApi.getDoctor(index);
      return result;
    } catch (e) {
      throw e;
    }
  }
}
