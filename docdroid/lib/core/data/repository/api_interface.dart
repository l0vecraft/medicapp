import 'package:docdroid/core/models/login_request.dart';
import 'package:docdroid/core/models/user.dart';

abstract class ApiInterface {
  Future<User> getUserFromToken(String token);
  Future<LoginRequest> login(User user);
  Future<void> logOut(String token);
}
