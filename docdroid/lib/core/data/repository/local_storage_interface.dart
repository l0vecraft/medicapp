import 'package:docdroid/core/models/user.dart';

abstract class LocalStorageInterface {
  Future<String> getToken();
  Future saveToken(String token);
  Future<void> clearAllData();
  Future<User> saveUser(User user);
  Future<User> getUser();
}
