import 'package:docdroid/core/data/repository/api_interface.dart';
import 'package:docdroid/core/models/login_request.dart';
import 'package:docdroid/core/models/user.dart';

class ApiRepository implements ApiInterface {
  @override
  Future<User> getUserFromToken(String token) async {
    await Future.delayed(Duration(seconds: 2));
    if (token == 'AA11') {
      return User(
          age: 23,
          blood: 'AB',
          gender: 'Male',
          height: 177,
          name: 'prueba_user@hotmail.com',
          password: '1234',
          weight: 80);
    }
    if (token == "BB22") {
      return User(
          age: 23,
          blood: 'O+',
          gender: 'Female',
          height: 177,
          name: 'prueba_user@hotmail.com',
          password: '1234',
          weight: 80);
    }
  }

  @override
  Future<void> logOut(String token) async {
    print('removiendo la sesion');
  }

  @override
  Future<LoginRequest> login(User user) async {
    Future.delayed(Duration(seconds: 2));
    if (user.name == 'prueba_user@hotmail.com' && user.password == '1234') {
      return LoginRequest(
          token: 'AA11',
          user: User(
              age: 23,
              blood: 'AB',
              gender: 'Male',
              height: 177,
              name: 'prueba_user@hotmail.com',
              password: '1234',
              weight: 80));
    }
    if (user.name == 'prueba_user2@hotmail.com' && user.password == '1234') {
      return LoginRequest(
          token: 'BB22',
          user: User(
              age: 23,
              blood: 'O+',
              gender: 'Female',
              height: 177,
              name: 'prueba_user2@hotmail.com',
              password: '1234',
              weight: 80));
    }
  }
}
