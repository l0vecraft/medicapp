import 'package:docdroid/core/models/doctors.dart';

class FakeDoctorApi {
  List<Doctors> doctors = [
    Doctors(
        id: 1,
        name: 'Dr. Vivek Thakur',
        specialization: 'Pediatra',
        address: 'Dishant Hospital,Ring Road, Nagpur',
        distance: 0.7,
        score: 4.5,
        genero: 'Masculino',
        image: 'assets/images/doctor1.jpeg',
        reviews: 378),
    Doctors(
        id: 2,
        name: 'Dr. Sagar',
        specialization: 'Oftalmologo',
        address: 'Dishant Hospital,Ring Road, Nagpur',
        distance: 0.6,
        score: 4.5,
        genero: 'Masculino',
        image: 'assets/images/doctor2.jpg',
        reviews: 982),
    Doctors(
        id: 3,
        name: 'Dr. Ashis',
        specialization: 'Oncologo',
        address: 'Dishant Hospital,Ring Road, Nagpur',
        distance: 1.2,
        score: 4.5,
        genero: 'Femenino',
        image: 'assets/images/doctora1.jpg',
        reviews: 213),
    Doctors(
        id: 4,
        name: 'Dr. Guru Barapatre',
        specialization: 'General',
        address: 'Dishant Hospital,Ring Road, Nagpur',
        distance: 1.4,
        score: 4.5,
        genero: 'Masculino',
        image: 'assets/images/doctor3.jpg',
        reviews: 45),
    Doctors(
        id: 5,
        name: 'Dr. Elif Rodriguez',
        specialization: 'General',
        address: 'Dishant Hospital,Ring Road, Nagpur',
        distance: 1.6,
        score: 4.5,
        genero: 'Femenino',
        image: 'assets/images/doctora2.jpg',
        reviews: 568),
    Doctors(
        id: 6,
        name: 'Dr. Vivek Thakur',
        specialization: 'Pediatra',
        address: 'Dishant Hospital,Ring Road, Nagpur',
        distance: 1.8,
        score: 4.5,
        genero: 'Masculino',
        image: 'assets/images/doctor2.jpg',
        reviews: 298),
    Doctors(
        id: 7,
        name: 'Dr. Diksha Shen',
        specialization: 'Otorinonaringologia',
        address: 'Dishant Hospital,Ring Road, Nagpur',
        distance: 2.0,
        score: 4.5,
        genero: 'Femenino',
        image: 'assets/images/doctora3.jpg',
        reviews: 481)
  ];

  Future getDoctors() async {
    await Future.delayed(Duration(milliseconds: 600));
    return doctors;
  }

  Future getDoctor(int index) async {
    await Future.delayed(Duration(milliseconds: 600));
    return doctors[index];
  }
}
