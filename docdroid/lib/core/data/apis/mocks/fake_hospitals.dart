import 'package:docdroid/core/models/hospitals.dart';

class FakeHospitals {
  Future getAllHospitals() async {
    await Future.delayed(Duration(milliseconds: 600));
    return hospitales;
  }

  Future getHospital(int id) async {
    await Future.delayed(Duration(milliseconds: 600));
    return hospitales[id];
  }
}
