class Hospitals {
  int id;
  String name;
  String specialization;
  String address;
  double distance;
  double score;
  String image;

  Hospitals(
      {this.id,
      this.name,
      this.specialization,
      this.address,
      this.distance,
      this.score,
      this.image});
}

List<Hospitals> hospitales = [
  Hospitals(
      id: 1,
      name: "Jym Hospital",
      specialization: "Pediatria",
      address: "Dishant,Ring road, nagpur",
      distance: 0.7,
      score: 4.5,
      image: 'assets/images/hospital1.jpg'),
  Hospitals(
      id: 2,
      name: "Zed Hospital",
      specialization: "Psiquiatria",
      address: "Dishant,Ring road, nagpur",
      distance: 0.6,
      score: 4.5,
      image: 'assets/images/hospital2.jpg'),
  Hospitals(
      id: 3,
      name: "Tic Hospital",
      specialization: "Clinica",
      address: "Dishant,Ring road, nagpur",
      distance: 1.2,
      score: 4.5,
      image: 'assets/images/hospital3.jpg'),
  Hospitals(
      id: 4,
      name: "New Life Hospital",
      specialization: "Urgencia",
      address: "Dishant,Ring road, nagpur",
      distance: 1.4,
      score: 4.5,
      image: 'assets/images/hospital4.jpg'),
  Hospitals(
      id: 5,
      name: "Tribal source Hospital",
      specialization: "General",
      address: "Dishant,Ring road, nagpur",
      distance: 1.8,
      score: 4.5,
      image: 'assets/images/hospital5.jpg'),
  Hospitals(
      id: 6,
      name: "Care Too Hospital",
      specialization: "Clinica",
      address: "Dishant,Ring road, nagpur",
      distance: 2.0,
      score: 4.5,
      image: 'assets/images/hospital2.jpg'),
];
