class Services{
  String name;
  int price;

  Services(String name, int price){
    this.name = name;
    this.price = price;
  }
}

List<Services> servicesList = [
  Services("Weakly Checkout", 20),
  Services("Blood Exam", 10),
  Services("Sugar Exam", 10),
  Services("Vision Test", 30),
  Services("Critical Operation", 200),
  Services("General Medicine", 10),
];