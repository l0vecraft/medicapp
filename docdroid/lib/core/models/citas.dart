import 'package:docdroid/core/models/doctors.dart';
import 'package:docdroid/core/models/services.dart';
import 'package:docdroid/core/models/user.dart';

class Citas {
  String idDoctor;
  String idUser;
  Doctors doctor;
  User user;
  DateTime date;
  int hour;

  Citas(
      {this.idDoctor,
      this.user,
      this.date,
      this.doctor,
      this.hour,
      this.idUser});
}

Services servicios = Services('Revision general', 10);
