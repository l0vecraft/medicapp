class User {
  final String name;
  final String password;
  final int age;
  final String blood;
  final String gender;
  final double height;
  final double weight;

  User(
      {this.name,
      this.password,
      this.age,
      this.blood,
      this.gender,
      this.height,
      this.weight});
}

User user = User(
    name: 'Jitu Raut',
    password: '123456789',
    age: 24,
    blood: 'AB',
    gender: 'Male',
    height: 177,
    weight: 82);
