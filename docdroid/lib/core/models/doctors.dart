class Doctors {
  int id;
  String name;
  String specialization;
  String address;
  double distance;
  double score;
  String genero;
  String image;
  int reviews;

  Doctors(
      {this.id,
      this.name,
      this.specialization,
      this.address,
      this.distance,
      this.score,
      this.genero,
      this.image,
      this.reviews});
}
