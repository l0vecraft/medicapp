class DrugStore {
  final String name;
  final String specialization;
  final String address;
  final double distance;
  final double score;
  final String image;

  DrugStore(this.name,this.specialization,this.address,this.distance,this.score,this.image);
}

List<DrugStore> drugstore = [
  DrugStore("Jym Hospital", "Pediatria", "Dishant,Ring road, nagpur", 0.7, 4.5, 'assets/images/hospital1.jpg'),
  DrugStore("Zed Hospital", "Psiquiatria", "Dishant,Ring road, nagpur", 0.6, 4.5, 'assets/images/hospital2.jpg'),
  DrugStore("Tic Hospital", "Clinica", "Dishant,Ring road, nagpur", 1.2, 4.5, 'assets/images/hospital3.jpg'),
  DrugStore("New Life Hospital", "Urgencia", "Dishant,Ring road, nagpur", 1.4, 4.5, 'assets/images/hospital4.jpg'),
  DrugStore("Tribal source Hospital", "General", "Dishant,Ring road, nagpur", 1.8, 4.5, 'assets/images/hospital5.jpg'),
  DrugStore("Care Too Hospital", "Clinica", "Dishant,Ring road, nagpur", 2.0, 4.5, 'assets/images/hospital2.jpg'),
  DrugStore("C1 Pharmacy", "Pediatria", "Dishant,Ring road, nagpur", 0.7, 4.5, 'assets/images/hospital1.jpg'),
  DrugStore("Mohan Pharmacy", "Psiquiatria", "Dishant,Ring road, nagpur", 0.6, 4.5, 'assets/images/hospital2.jpg'),
  DrugStore("Tic Hospital", "Clinica", "Dishant,Ring road, nagpur", 1.2, 4.5, 'assets/images/hospital3.jpg'),
  DrugStore("New Life Hospital", "Urgencia", "Dishant,Ring road, nagpur", 1.4, 4.5, 'assets/images/hospital4.jpg'),
  DrugStore("Tribal source Hospital", "General", "Dishant,Ring road, nagpur", 1.8, 4.5, 'assets/images/hospital5.jpg'),
  DrugStore("Care Too Hospital", "Clinica", "Dishant,Ring road, nagpur", 2.0, 4.5, 'assets/images/hospital2.jpg'),
  

];