import 'package:docdroid/core/models/user.dart';

class LoginRequest {
  String token;
  User user;

  LoginRequest({this.token, this.user});
}
