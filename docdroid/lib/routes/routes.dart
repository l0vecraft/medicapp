import 'package:docdroid/core/controllers/auth/authBinding.dart';
import 'package:docdroid/core/controllers/citas/citas_binding.dart';
import 'package:docdroid/core/controllers/doctors/doctor_binding.dart';
import 'package:docdroid/core/controllers/hospitals/hospitals_bindigs.dart';
import 'package:docdroid/core/controllers/login/loginBinding.dart';
import 'package:docdroid/ui/views/hospitals.dart';

import 'all_routes.dart';

class MedicRoutes {
  final pages = [
    GetPage(
      name: '/',
      page: () => HomeView(),
      binding: AuthBinding(),
    ),
    GetPage(name: '/login', page: () => LoginView(), binding: LoginBinding()),
    GetPage(name: '/register', page: () => RegisterView()),
    GetPage(name: '/menu', page: () => PrincipalMenu(), binding: AuthBinding()),
    GetPage(name: '/findDoctor', page: () => FindDoctor()),
    GetPage(
        name: '/hospital',
        page: () => HospitalsViews(),
        binding: HospitalsBindings()),
    GetPage(name: '/drugStore', page: () => PrincipalMenu()),
    GetPage(name: '/emergency', page: () => PrincipalMenu()),
    GetPage(name: '/appointments', page: () => PrincipalMenu()),
    GetPage(
        name: '/doctors', page: () => ListDoctors(), binding: DoctorBinding()),
    GetPage(name: '/dashboards', page: () => DashBoardView()),
    GetPage(
        name: '/addCita',
        page: () => CalendarWidget(),
        bindings: [DoctorBinding(), CitasBinding()]),
    GetPage(
        name: '/doctorInfo',
        page: () => DoctorInfo(),
        binding: DoctorBinding()),
  ];
}
