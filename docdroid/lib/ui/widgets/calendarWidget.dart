import 'package:docdroid/core/controllers/doctors/doctor.dart';
import 'package:docdroid/core/models/citas.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';

import '../../core/controllers/citas/citas.dart';

class CalendarWidget extends StatefulWidget {
  final DoctorController _doctorController = Get.find<DoctorController>();
  @override
  _CalendarWidgetState createState() => _CalendarWidgetState();
}

class _CalendarWidgetState extends State<CalendarWidget> {
  DateFormat _format = DateFormat('dd-MM-yyyy HH:mm');
  DateTime _selectedDate = DateTime.now();
  String _formatDate;
  Citas _nuevaCitas;
  CitasController _citas = Get.find<CitasController>();

  CalendarController _calendarController;
  @override
  void initState() {
    _calendarController = CalendarController();
    super.initState();
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  _showTime(BuildContext context) async {
    final hour = DateTime.now();
    return showTimePicker(
        context: context,
        initialTime: TimeOfDay(hour: hour.hour, minute: hour.minute));
  }

  _buildCitas(int i, Citas c, DoctorController bloc) {
    return Obx(
      () => Container(
          padding: const EdgeInsets.only(left: 8.0),
          child: ListTile(
            title: Text('${bloc.doctorsList[i].specialization}'),
            //title: Text('${widget.doctor.name}'),
            subtitle: Text('${_format.format(_citas.listCitas[i].date)}'),
            trailing: CircleAvatar(
              radius: 20,
              backgroundImage: AssetImage(
                bloc.doctor.value.image,
              ),
            ),
            onTap: () =>
                Navigator.pushNamed(context, 'citaDetails', arguments: [c, i]),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<DoctorController>(
        init: DoctorController(),
        builder: (bloc) => Stack(
          children: <Widget>[
            AppBar(
              title: Text('Event List'),
              centerTitle: true,
              backgroundColor: Colors.blueAccent,
            ),
            Positioned(
              bottom: 0.0,
              child: Container(
                height: 500.0,
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 100, left: 20),
              child: Container(
                width: 320,
                decoration: BoxDecoration(color: Colors.white),
                child: TableCalendar(
                  calendarController: _calendarController,
                  onDayLongPressed: (date, x) async {
                    var time = await _showTime(context);
                    if (time == null) return;
                    setState(() {
                      _selectedDate = DateTime(date.year, date.month, date.day,
                          time.hour, time.minute);
                      _formatDate = _format.format(_selectedDate);
                      _nuevaCitas =
                          // Citas(bloc.doctor.value, _selectedDate, servicios);
                          Citas(
                              doctor: bloc.doctor.value,
                              date: _selectedDate,
                              idDoctor: bloc.doctor.value.id.toString(),
                              idUser: '1',
                              hour: 12);
                    });
                    _citas.addCita(_nuevaCitas);
                    print('$_formatDate ${bloc.doctor.value.name}');
                    // x.add('Evento');
                    // print(x.length);
                  },
                ),
              ),
            ),
            _citas.listCitas.length == 0
                ? Container()
                : Positioned(
                    bottom: 10,
                    child: Container(
                      height: 300,
                      width: 360,
                      child: ListView.builder(
                          itemCount: _citas.listCitas.length,
                          itemBuilder: (context, i) =>
                              _buildCitas(i, _nuevaCitas, bloc)),
                    ))
          ],
        ),
      ),
    );
  }
}
