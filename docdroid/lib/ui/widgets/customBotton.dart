import 'package:docdroid/ui/const.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatefulWidget {
  final String title;
  final Function onPress;

  CustomButton({this.title, @required this.onPress});

  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onPress,
      child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(10.0),
          child: Container(
            height: 45,
            width: 300,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: AppStyles.colors,
                    begin: Alignment.centerLeft,
                    end: Alignment.topRight),
                borderRadius: BorderRadius.circular(10.0)),
            child: Center(
                child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(
                widget.title,
                style: AppStyles.buttonText,
              ),
            )),
          )),
    );
  }
}
