import 'package:docdroid/ui/const.dart';
import 'package:flutter/material.dart';

class DrawerItem extends StatelessWidget {
  final String title;
  final String ruta;

  DrawerItem({this.title,this.ruta});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(title.toUpperCase(),style: AppStyles.drawerItem,),
      onTap: (){
        Navigator.pushNamed(context, ruta);
      },
    );
  }
}
