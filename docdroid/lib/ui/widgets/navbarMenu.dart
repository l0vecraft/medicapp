import 'package:docdroid/ui/const.dart';
import 'package:flutter/material.dart';

class NavBarMenu extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final IconData icon;
  final Function onPress;
  final Icon trailing;
  final Function trailingPress;
  final TextStyle style;

  NavBarMenu(
      {this.title,
      this.icon,
      this.onPress,
      this.style,
      this.trailing,
      this.trailingPress});

  Widget _customItems() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: IconButton(
            iconSize: 25,
            icon: Icon(
              icon,
              color: Colors.white,
            ),
            onPressed: onPress,
          ),
        ),
        Container(
          padding: EdgeInsets.only(top: 15, right: 12),
          alignment: Alignment.center,
          child: Text(
            title ?? '',
            style: TextStyle(color: Colors.white, fontSize: 26),
          ),
        ),
        Container()
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 80,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: AppStyles.colors,
                begin: Alignment.centerLeft,
                end: Alignment.centerRight)),
        child: icon != null
            ? _customItems()
            : Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 110, top: 10),
                    child: Center(
                      child: Text(
                        title,
                        style: TextStyle(
                            fontSize: 30,
                            color: Colors.white,
                            fontFamily: 'Poppin'),
                      ),
                    ),
                  )
                ],
              ));
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(70);
}
