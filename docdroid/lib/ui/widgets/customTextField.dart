import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final String text;
  final TextEditingController controller;
  final bool obscure;

  CustomTextField({this.text, this.obscure, this.controller});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
      child: TextField(
        controller: controller,
        cursorColor: Color(0xff03ffbc),
        decoration: InputDecoration(
            hintText: text,
            contentPadding: EdgeInsets.only(
              left: 18.0,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
            )),
        obscureText: obscure,
      ),
    );
  }
}
