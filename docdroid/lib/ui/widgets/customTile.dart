import 'package:flutter/material.dart';

class CustomTile extends StatelessWidget {
  final IconData icon;
  final String title;
  final Function onTap;

  CustomTile({this.title,this.icon,this.onTap});

  _buildFirstPart(){
    return Padding(
      padding: const EdgeInsets.only(left:8.0),
      child: Row(
        children: <Widget>[
          Icon(icon, color: Colors.cyan,size:25,),
          SizedBox(width: 5,),
          Text(title, style: TextStyle(color: Colors.black,fontSize: 15),),
        ],
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.cyanAccent,
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical:12.0),
        child: Container(
          height: 60.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: Colors.cyan[200]
            )
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _buildFirstPart(),
              Row(
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.arrow_forward_ios),
                    iconSize: 20,
                    color: Colors.cyan,
                    onPressed: (){},
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}