import 'package:flutter/material.dart';

class DropDownFields extends StatefulWidget {
  final List<String> values;

  DropDownFields({this.values});

  @override
  _DropDownFieldsState createState() => _DropDownFieldsState();
}

class _DropDownFieldsState extends State<DropDownFields> {
  String _doctor = 'Medico especialista';

  List<Widget> _dropItems(){
    return widget.values.map((item)=>DropdownMenuItem(
      value: item,
      child: Text(item),
    )).toList();
  }
  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: _doctor,
      items: _dropItems(),
      onChanged: (value){
        setState(() {
          _doctor = _doctor != null?value:'Medico especialista';
        });
      },
    );
  }
}
