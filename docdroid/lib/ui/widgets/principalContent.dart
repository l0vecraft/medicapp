import 'package:docdroid/ui/const.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class PrincipalContent extends StatelessWidget {
  final String img;
  final String title;

  PrincipalContent({this.img, this.title});

  Widget _cardItem(String img, String title) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          side: BorderSide(color: AppStyles.colors[1])),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 20),
            height: 140,
            child: SvgPicture.asset(img),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Text(
              title,
              style: AppStyles.subtitle,
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0, left: 10.0),
      child: _cardItem(img, title),
    );
  }
}
