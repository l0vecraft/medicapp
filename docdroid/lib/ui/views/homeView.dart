import 'package:docdroid/core/controllers/auth/authController.dart';
import 'package:docdroid/routes/all_routes.dart';
import 'package:docdroid/ui/widgets/customBotton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeView extends StatelessWidget {
  // AuthController _auth = Get.find();
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                child: Hero(
                    tag: 'logo',
                    child: SvgPicture.asset(
                      'assets/images/logo.svg',
                    )),
              ),
              SizedBox(
                height: 90,
              ),
              CustomButton(
                title: 'Sign In',
                onPress: () {
                  Get.toNamed('login');
                },
              ),
              SizedBox(
                height: 20,
              ),
              CustomButton(
                title: 'Sign Up',
                onPress: () {
                  Get.toNamed('register');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
