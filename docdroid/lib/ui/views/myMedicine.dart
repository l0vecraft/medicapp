import 'package:docdroid/ui/widgets/navbarMenu.dart';
import 'package:flutter/material.dart';

class MyMedicine extends StatefulWidget {
  @override
  _MyMedicineState createState() => _MyMedicineState();
}

class _MyMedicineState extends State<MyMedicine> {
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      body: Column(
        children: <Widget>[
          NavBarMenu(
            icon: Icons.arrow_back_ios,
            title: 'My Medicine',
            onPress: () => Navigator.pop(context),
          ),
          Expanded(
            child: CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  floating: true,
                  backgroundColor: Colors.transparent,
                  expandedHeight: 150,
                  flexibleSpace: FlexibleSpaceBar(
                    title: Text('Medicinas'),
                    background: Image.asset('assets/images/medicina.png'),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
