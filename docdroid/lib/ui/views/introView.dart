import 'package:flutter/material.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';

class IntroView extends StatelessWidget {
  final pages = [
    PageViewModel(
    pageColor: Colors.white,//esto es obligatorio
    body: Text('Find doctors for a particular problem'),
    title: Text('Doctors'),
    mainImage: Image.asset('assets/images/doctor-intro.png',height: 285,width: 285,alignment: Alignment.center,),
    titleTextStyle: TextStyle(color: Colors.cyan, fontFamily: 'Poppin'),
    bodyTextStyle: TextStyle(color: Colors.cyan, fontFamily: 'Poppin')
  ),
  PageViewModel(
    pageColor: Colors.white,
    body: Text('Alopathic, Ayurvedic and all type of medicine can bought from here'),
    title: Text('Medicine'),
    mainImage: Image.asset('assets/images/medicine-intro.png',height: 285,width: 285,alignment: Alignment.center,),
    titleTextStyle: TextStyle(color: Colors.cyan,fontFamily: 'Poppin'),
    bodyTextStyle: TextStyle(color: Colors.cyan,fontFamily: 'Poppin')
  ),
  PageViewModel(
    pageColor: Colors.white,
    body: Text('Book appointment and get best tretment one for tap'),
    title: Text('Appointment'),
    mainImage: Image.asset('assets/images/appointment-intro.png',height: 285,width: 285,alignment: Alignment.center,),
    titleTextStyle: TextStyle(color: Colors.cyan,fontFamily: 'Poppin'),
    bodyTextStyle: TextStyle(color: Colors.cyan,fontFamily: 'Poppin')
  ),
  
  ];
  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context)=>IntroViewsFlutter(
        pages,
        showBackButton: false,
        showNextButton: false,
        //pageButtonTextStyles: TextStyle(color: Colors.cyan),
        pageButtonsColor: Colors.cyan,
        onTapDoneButton: ()=> Navigator.pushNamed(context,'menu'),
      ),
    );
  }
}