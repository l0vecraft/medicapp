import 'package:docdroid/ui/widgets/customBotton.dart';
import 'package:docdroid/ui/widgets/customTextField.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../const.dart';

class RegisterView extends StatelessWidget {
  _fields() {
    return [];
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScopeNode _currentFocus = FocusScope.of(context);
          if (!_currentFocus.hasPrimaryFocus) _currentFocus.unfocus();
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 30.0),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.arrow_back_ios),
                    color: Colors.black,
                    iconSize: 30,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 14.0),
                    child: Center(
                      child: Hero(
                        tag: 'logo',
                        child: Container(
                            height: 170,
                            child: SvgPicture.asset('assets/images/logo.svg')),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 18.0),
                    child: Text(
                      'Sign Up',
                      style: AppStyles.titleStyle,
                    ),
                  ),
                  SizedBox(
                    height: 40.0,
                  ),
                  CustomTextField(
                    text: 'Full Name',
                    obscure: false,
                  ),
                  CustomTextField(
                    text: 'E-mail',
                    obscure: false,
                  ),
                  CustomTextField(
                    text: 'Phone Number',
                    obscure: false,
                  ),
                  CustomTextField(
                    text: 'Password',
                    obscure: true,
                  ),
                  CustomTextField(
                    text: 'Confirm Password',
                    obscure: false,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: size.width / 9, vertical: size.height / 98),
                    child: CustomButton(
                      title: 'Sign Up',
                      onPress: () {
                        Navigator.pushReplacementNamed(context, 'menu');
                      },
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
