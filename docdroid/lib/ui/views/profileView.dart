import 'package:docdroid/core/models/user.dart';
import 'package:docdroid/ui/const.dart';
import 'package:docdroid/ui/widgets/customTile.dart';
import 'package:docdroid/ui/widgets/navbarMenu.dart';
import 'package:flutter/material.dart';

class ProfileView extends StatefulWidget {
  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  _buildContent(User u) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 60),
          child: Text(
            u.name,
            style: AppStyles.titleModal,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _buildItems("Age", u.age),
              _buildItems("Blood", u.blood),
              _buildItems("Gender", u.gender),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40.0, vertical: 15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _buildItems("Height", u.height),
              _buildItems("Weight", u.weight),
            ],
          ),
        ),
      ],
    );
  }

  _buildItems(String title, dynamic data) {
    return Column(
      children: <Widget>[
        Text(
          title,
          style: TextStyle(fontSize: 20, color: Colors.black),
        ),
        SizedBox(
          height: 5.0,
        ),
        Text(
          "$data",
          style: TextStyle(color: Colors.grey[500]),
        )
      ],
    );
  }

  _buildOptions() {
    return ListView(
      children: <Widget>[
        CustomTile(
          title: 'Medical History',
          icon: Icons.hotel,
          onTap: () {},
        ),
        CustomTile(
          title: 'Favourite Doctor',
          icon: Icons.favorite_border,
          onTap: () {},
        ),
        CustomTile(
          title: 'Insurance',
          icon: Icons.enhanced_encryption,
        ),
      ],
    );
  }

  _buildGradient(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: AppStyles.colors,
                begin: Alignment.topLeft,
                end: Alignment.bottomRight)),
        child: _buildTopNavBar(context));
  }

  _buildTopNavBar(BuildContext context) {
    return Column(
      children: <Widget>[
        NavBarMenu(
          title: 'User Profile',
          icon: Icons.arrow_back_ios,
          onPress: () => Navigator.pop(context),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
    return Scaffold(
      key: key,
      body: Stack(
        fit: StackFit.loose,
        children: <Widget>[
          _buildGradient(context),
          Positioned(
            bottom: 0.0,
            child: Container(
              height: 540.0,
              width: MediaQuery.of(context).size.width * 0.95,
              margin: EdgeInsets.only(left: 10.0),
              color: Colors.white,
              child: _buildContent(user),
            ),
          ),
          Positioned(
            top: 120,
            left: 130,
            child: CircleAvatar(
              radius: 50,
              backgroundImage: AssetImage('assets/images/user.png'),
            ),
          ),
          Positioned(
            bottom: 10,
            child: Container(
              padding: EdgeInsets.only(left: 25),
              height: 300,
              width: MediaQuery.of(context).size.width * 0.92,
              child: _buildOptions(),
            ),
          )
        ],
      ),
    );
  }
}
