import 'package:docdroid/core/controllers/doctors/doctor.dart';
import 'package:docdroid/routes/all_routes.dart';
import 'package:docdroid/ui/const.dart';
import 'package:docdroid/ui/widgets/Dropdown.dart';
import 'package:docdroid/ui/widgets/customBotton.dart';
import 'package:docdroid/ui/widgets/navbarMenu.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';

class FindDoctor extends StatefulWidget {
  @override
  _FindDoctorState createState() => _FindDoctorState();
}

class _FindDoctorState extends State<FindDoctor> {
  List<String> _especialista = [
    "Pediatra",
    "Oncologo",
    "Otorinonaringologo",
    "General",
    "Oftalmologo"
  ];
  List<String> _generos = ['Masculino', 'Femenino'];
  String _doctor = "Medico Especialista";
  String _genero = "Genero";
  String _ubicacion = "Ubicacion actual";
  String _fecha = "Fecha";

  Widget _dropDownMenu(
      String initialValue, List<String> items, Function onValueChanged) {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ListView(
          children: <Widget>[
            NavBarMenu(
              title: 'Docdroid',
              style: AppStyles.navTitle,
              icon: Icons.arrow_back_ios,
              onPress: () {
                Navigator.pop(context);
              },
            ),
            Container(
                padding: EdgeInsets.only(left: 20, top: 25.0),
                child: Text('Find Doctor', style: AppStyles.titleStyle)),
            SizedBox(
              height: 8.0,
            ),
            Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                child: CustomDropDown(
                  initialValue: _doctor,
                  items: _especialista,
                  onChanged: (value) {
                    setState(() {
                      _doctor = value;
                    });
                  },
                )),
            Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                child: CustomDropDown(
                  initialValue: _ubicacion,
                  items: _especialista,
                  onChanged: (value) {},
                )),
            Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                child: CustomDropDown(
                  initialValue: _fecha,
                  items: _especialista,
                  onChanged: (value) {},
                )),
            Padding(
                padding: const EdgeInsets.only(top: 20.0, bottom: 50),
                child: CustomDropDown(
                  initialValue: _genero,
                  items: _generos,
                  onChanged: (value) {
                    setState(() {
                      _genero = value;
                    });
                  },
                )),
            GetBuilder<DoctorController>(
              init: DoctorController(),
              builder: (DoctorController bloc) => Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: CustomButton(
                  title: 'Search',
                  onPress: () {
                    Get.toNamed('/doctors');
                    // print(bloc.doctorsList
                    //     .where((doc) => doc.genero == _genero ?? '')
                    //     .where((doc) => doc.specialization == _doctor ?? ''));
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CustomDropDown extends StatefulWidget {
  final String initialValue;
  final List<String> items;
  final Function onChanged;

  CustomDropDown({this.initialValue, this.items, this.onChanged});

  @override
  _CustomDropDownState createState() => _CustomDropDownState();
}

class _CustomDropDownState extends State<CustomDropDown> {
  List<Widget> _buildDropDownItems(List<String> items) {
    return items
        .map((item) => DropdownMenuItem(
              value: item,
              child: Text(item),
            ))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 22.0, vertical: 15),
      child: DropdownButton(
          hint: Text(widget.initialValue),
          icon: Icon(Icons.keyboard_arrow_down),
          isExpanded: true,
          isDense: true,
          style: TextStyle(fontSize: 20.0, color: Colors.black),
          items: _buildDropDownItems(widget.items),
          onChanged: widget.onChanged),
    );
  }
}
