import 'dart:io';
import 'package:docdroid/core/controllers/detectorController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class DashBoardView extends StatefulWidget {
  @override
  _DashBoardViewState createState() => _DashBoardViewState();
}

class _DashBoardViewState extends State<DashBoardView> {
  final picker = ImagePicker();
  File _image;
  bool _isChange = false;

  _pickImage(DetectorController bloc) async {
    var pick = await picker.getImage(source: ImageSource.gallery);
    _image = File(pick.path);
    await bloc.classifyImage(_image.path);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GetBuilder<DetectorController>(
      init: DetectorController(),
      builder: (bloc) => Scaffold(
        appBar: AppBar(),
        body: Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: size.width / 1.2,
                  height: size.height / 2.5,
                  child: !_isChange
                      ? Center(
                          child: Text('Seleccione una imagen para analizar'))
                      : Container(
                          child: Column(
                            children: [
                              Container(
                                child: Image.file(_image),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Obx(() => Text('${bloc.output[0]["label"]}'))
                            ],
                          ),
                        ),
                ),
                Padding(
                  padding: const EdgeInsets.all(13.0),
                  child: RaisedButton(
                    onPressed: () async {
                      await _pickImage(bloc);
                      setState(() {
                        _isChange = true;
                      });
                      // bloc.loading = false;
                    },
                    color: Colors.blue,
                    child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 4.0, horizontal: 10),
                        child: Text(
                          'Detectar',
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        )),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
