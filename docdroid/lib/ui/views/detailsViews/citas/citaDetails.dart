import 'package:docdroid/core/models/citas.dart';
import 'package:intl/intl.dart';
import 'package:docdroid/ui/const.dart';
import 'package:docdroid/ui/widgets/navbarMenu.dart';
import 'package:flutter/material.dart';

class CitaDetails extends StatefulWidget {
  final Citas citas;
  final int id;
  CitaDetails({this.citas, this.id});

  @override
  _CitaDetailsState createState() => _CitaDetailsState();
}

class _CitaDetailsState extends State<CitaDetails> {
  DateFormat _dateFormat = DateFormat('dd - MM - yyyy HH:mm');

  _buildDoctorProfile() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          CircleAvatar(
            radius: 40,
            backgroundImage: AssetImage('${widget.citas.doctor.image}'),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text(
                  '${widget.citas.doctor.name}',
                  style: AppStyles.titleModal,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0, top: 10, bottom: 8),
                child:
                    Text('${widget.citas.doctor.specialization}'.toUpperCase()),
              ),
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Icon(
                      Icons.location_on,
                      color: Colors.cyan,
                    ),
                  ),
                  Text('${widget.citas.doctor.address}')
                ],
              )
            ],
          )
        ],
      ),
    );
  }

  _buildItem(IconData icon, String title) {
    return ListTile(
      title: Text(
        title,
        style: TextStyle(color: Colors.grey[500]),
      ),
      leading: Icon(
        icon,
        color: Colors.cyan,
      ),
      contentPadding: EdgeInsets.symmetric(horizontal: 8),
    );
  }

  _buildCancelButton(String title) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 18.0, horizontal: 25),
      child: MaterialButton(
        onPressed: () {
          //citasController.cancelCita(widget.id);
          Navigator.pop(context);
        },
        child: Text(
          title,
          style: TextStyle(color: Colors.grey, fontSize: 20),
        ),
        color: Colors.white,
        padding: EdgeInsets.all(14),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
      ),
    );
  }

  _buildServices() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text(
              '${widget.citas.service.name}',
              style: TextStyle(fontSize: 17),
            ),
            trailing: Text(
              '\$ ${widget.citas.service.price}',
              style: TextStyle(color: Colors.grey[500]),
            ),
          ),
          Divider(),
          ListTile(
            title: Text(
              'Total',
              style: TextStyle(fontSize: 17),
            ),
            trailing: Text(
              '\$ ${widget.citas.service.price}',
            ),
          )
        ],
      ),
    );
  }

  _buildContent() {
    return Container(
      width: 200,
      decoration: BoxDecoration(
          border: Border.all(color: Colors.tealAccent[400]),
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildItem(
              Icons.calendar_today, _dateFormat.format(widget.citas.date)),
          SizedBox(
            height: 15,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text(
              'Services',
              style: TextStyle(
                  fontFamily: 'Poppin',
                  color: Colors.cyan[600],
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Divider(),
          _buildServices()
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            NavBarMenu(
              icon: Icons.arrow_back_ios,
              title: 'Appoint. Details',
              onPress: () => Navigator.pop(context),
            ),
            Expanded(
              child: ListView(
                children: <Widget>[
                  _buildDoctorProfile(),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: _buildContent(),
                  ),
                  _buildCancelButton('Cancel Appointment')
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
