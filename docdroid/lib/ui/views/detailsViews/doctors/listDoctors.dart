import 'package:docdroid/core/controllers/doctors/doctor.dart';
import 'package:docdroid/core/models/doctors.dart';
import 'package:docdroid/ui/const.dart';
import 'package:docdroid/ui/views/detailsViews/doctors/doctorDetails.dart';
import 'package:docdroid/ui/widgets/navbarMenu.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListDoctors extends StatelessWidget {
  final DoctorController _doctorController = Get.find<DoctorController>();

  Widget _buildTitleItem(Doctors doc) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Text(doc.name),
              flex: 2,
            ),
            Flexible(
                child: Row(
              children: <Widget>[
                Icon(Icons.near_me, color: Colors.grey),
                Text(
                  '${doc.distance} Km',
                  style: TextStyle(color: Colors.grey[400]),
                )
              ],
            ))
          ],
        ),
        Row(
          children: <Widget>[
            Icon(
              Icons.location_on,
              color: Colors.blue,
            ),
            Text(
              doc.address,
              style: TextStyle(fontSize: 13, fontFamily: 'Poppin'),
            ),
          ],
        )
      ],
    );
  }

  Widget _buildSubtitle(Doctors doc) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Text(doc.specialization),
          flex: 3,
        ),
        Flexible(
            child: Row(
          children: <Widget>[
            Icon(Icons.star_border, color: Colors.blueAccent),
            Text(
              ' ${doc.score}',
              style: TextStyle(
                  color: Colors.blueAccent,
                  fontSize: 15,
                  fontWeight: FontWeight.bold),
            )
          ],
        ))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: NavBarMenu(
          title: 'Docdroid',
          style: AppStyles.navTitle,
          icon: Icons.arrow_back_ios,
          onPress: () => Navigator.pop(context),
        ),
        body: Obx(() => _doctorController.doctorsList.length == 0
            ? Center(child: CircularProgressIndicator())
            : Container(
                padding: EdgeInsets.only(top: 10.0),
                child: ListView.builder(
                  itemCount: _doctorController.doctorsList.length,
                  itemBuilder: (context, i) => //para que se vea con mas estilo
                      Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: ListTile(
                      leading: CircleAvatar(
                        radius: 25,
                        backgroundImage: AssetImage(
                          _doctorController.doctorsList[i].image,
                        ),
                      ),
                      title: _buildTitleItem(_doctorController.doctorsList[i]),
                      subtitle:
                          _buildSubtitle(_doctorController.doctorsList[i]),
                      isThreeLine: true,
                      onTap: () async {
                        await _doctorController.getDoctor(i);
                        showModalBottomSheet(
                            context: context,
                            builder: (context) => DoctorDetails(),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(40),
                                    topRight: Radius.circular(40))));
                      },
                    ),
                  ),
                ))));
  }
}
