import 'package:docdroid/core/controllers/doctors/doctor.dart';
import 'package:docdroid/ui/const.dart';
import 'package:docdroid/ui/widgets/customBotton.dart';
import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:get/get.dart';

class DoctorDetails extends StatefulWidget {
  @override
  _DoctorDetailsState createState() => _DoctorDetailsState();
}

class _DoctorDetailsState extends State<DoctorDetails> {
  Widget _buildTitleContent(DoctorController bloc) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 100.0),
            child: CircleAvatar(
              backgroundImage: AssetImage(bloc.doctor.value.image),
              radius: 50,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(
                    Icons.star_border,
                    color: Colors.blueAccent,
                    size: 40,
                  ),
                  Text(
                    '${bloc.doctor.value.score}',
                    style: TextStyle(
                        color: Colors.blueAccent,
                        fontSize: 25,
                        fontFamily: 'Poppin',
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              Text(
                '(${bloc.doctor.value.reviews} Reviews)',
                style: AppStyles.subtitle,
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _buildTitlesAndSubtitles(DoctorController bloc) {
    return Padding(
      padding: EdgeInsets.only(top: 20.0),
      child: Column(
        children: <Widget>[
          Text(
            '${bloc.doctor.value.name}',
            style: AppStyles.titleModal,
          ),
          Text(
            '${bloc.doctor.value.specialization}',
            style: AppStyles.subTitleModal,
          ),
          Text(
            '${bloc.doctor.value.address}',
            style: AppStyles.subTitleModal,
          )
        ],
      ),
    );
  }

  Widget _buildMiddleButtons(DoctorController bloc) {
    return Padding(
      padding: EdgeInsets.only(top: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _buildItems(Icons.camera_alt, 'Photos'),
          _buildItems(Icons.near_me, bloc.doctor.value.distance.toString()),
          _buildItems(Icons.call, 'Call')
        ],
      ),
    );
  }

  Widget _buildFinalContent() {
    return Padding(
      padding: EdgeInsets.only(top: 20),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.location_on, color: Colors.blueAccent),
            title: Text("Doctor's Details"),
            trailing: Icon(
              Icons.keyboard_arrow_right,
              size: 35,
              color: Colors.grey,
            ),
            onTap: () {
              Get.toNamed('/doctorInfo');
            },
          ),
          CustomButton(
            title: 'Book Appointment',
            onPress: () {
              Get.toNamed('/addCita');
            },
          )
        ],
      ),
    );
  }

  Widget _buildItems(IconData icon, String text) {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            icon,
            color: Colors.blue[600],
          ),
          iconSize: 30,
          onPressed: () {},
        ),
        Text(
          text,
          style: TextStyle(color: Colors.blue[600]),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DoctorController>(
      init: DoctorController(),
      builder: (bloc) => Container(
        child: Column(
          children: <Widget>[
            _buildTitleContent(bloc),
            _buildTitlesAndSubtitles(bloc),
            _buildMiddleButtons(bloc),
            _buildFinalContent()
          ],
        ),
      ),
    );
  }
}
