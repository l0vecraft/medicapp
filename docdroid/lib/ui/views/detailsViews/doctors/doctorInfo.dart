import 'package:docdroid/core/controllers/doctors/doctor.dart';
import 'package:docdroid/routes/all_routes.dart';
import 'package:docdroid/ui/const.dart';
import 'package:docdroid/ui/widgets/navbarMenu.dart';
import 'package:flutter/material.dart';

class DoctorInfo extends StatelessWidget {
  final DoctorController _doctorController = Get.find<DoctorController>();

  Widget _buildTitleContent() {
    return Row(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 30, left: 20),
          child: CircleAvatar(
            backgroundImage: AssetImage(_doctorController.doctor.value.image),
            radius: 50,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 18.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                _doctorController.doctor.value.name,
                style: AppStyles.titleModal,
                overflow: TextOverflow.clip,
              ),
              Text(
                _doctorController.doctor.value.specialization,
                style: AppStyles.subTitleModal,
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _buildAbout() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 11.0, vertical: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'About',
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          ),
          Text(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce rutrum accumsan elit, at semper mi egestas eget. Sed maximus malesuada elit, at congue velit sagittis at. Praesent vel suscipit eros, nec hendrerit lectus. Ut eu orci pharetra turpis sollicitudin cursus quis sit amet ipsum. Vestibulum faucibus lorem libero, et bibendum massa imperdiet ac. Phasellus imperdiet dignissim massa. Cras egestas neque eget purus luctus molestie.',
            style: TextStyle(color: Colors.grey),
          ),
        ],
      ),
    );
  }

  Widget _buildAddAndTime() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Text('Address & Timing',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
          ),
          _buildItems(
              Icons.location_on, _doctorController.doctor.value.address),
          _buildItems(Icons.date_range, '8:00 AM to 4:00 PM'),
        ],
      ),
    );
  }

  Widget _buildEndContent() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Text('Certificacionts',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
          ),
          _buildItems(Icons.done, 'BAMS'),
          _buildItems(Icons.done, 'MBBS'),
        ],
      ),
    );
  }

  Widget _buildItems(IconData icon, String text) {
    return ListTile(
      leading: Icon(
        icon,
        color: Colors.blueAccent,
      ),
      title: Text(text,
          style: TextStyle(
            color: Colors.grey,
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: NavBarMenu(
            style: AppStyles.navTitle,
            icon: Icons.arrow_back_ios,
            title: 'Doc Info',
            onPress: () => Navigator.pop(context)),
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _buildTitleContent(),
                Divider(),
                _buildAbout(),
                Divider(),
                _buildAddAndTime(),
                Divider(),
                _buildEndContent(),
                Divider()
              ],
            ),
          ),
        ));
  }
}
