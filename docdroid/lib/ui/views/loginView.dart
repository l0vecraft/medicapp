import 'package:docdroid/core/controllers/login/loginController.dart';
import 'package:docdroid/ui/const.dart';
import 'package:docdroid/ui/widgets/customBotton.dart';
import 'package:docdroid/ui/widgets/customTextField.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  bool _isCheck = false;
  final LoginController _loginController = Get.find<LoginController>();

  void login() async {
    final res = await _loginController.login();
    if (res) {
      Get.toNamed('/menu');
    } else {
      Get.snackbar('Error', 'Error en los datos por favor revise.');
    }
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScopeNode _currentFocus = FocusScope.of(context);
          if (!_currentFocus.hasPrimaryFocus) _currentFocus.unfocus();
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 30.0),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.arrow_back_ios),
                    color: Colors.black,
                    iconSize: 30,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 14.0),
                    child: Center(
                      child: Hero(
                        tag: 'logo',
                        child: Container(
                            height: 260,
                            child: SvgPicture.asset('assets/images/logo.svg')),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 18.0),
                    child: Text(
                      'Sign In',
                      style: AppStyles.titleStyle,
                    ),
                  ),
                  SizedBox(
                    height: 40.0,
                  ),
                  CustomTextField(
                    text: 'E-mail',
                    obscure: false,
                    controller: _loginController.usernameController,
                  ),
                  CustomTextField(
                    text: 'Password',
                    obscure: true,
                    controller: _loginController.passwordController,
                  ),
                  Row(
                    children: <Widget>[
                      Checkbox(
                        value: _isCheck,
                        activeColor: Colors.blue,
                        onChanged: (value) {
                          setState(() {
                            _isCheck = value;
                          });
                        },
                      ),
                      Text(
                        'Remember',
                        style: AppStyles.fieldsStyle,
                      )
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: size.width / 9),
                    child: CustomButton(title: 'Sign In', onPress: login),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Center(child: Text('Or', style: AppStyles.fieldsStyle)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Center(
                      child: InkWell(
                    onTap: () {
                      Navigator.popAndPushNamed(context, 'register');
                    },
                    child: RichText(
                      text: TextSpan(children: [
                        TextSpan(
                            text: "Don't have account?  ",
                            style: AppStyles.fieldsStyle),
                        TextSpan(
                            text: "Create account",
                            style: TextStyle(color: Colors.blue))
                      ]),
                    ),
                  )),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
