import 'package:docdroid/core/controllers/auth/authController.dart';
import 'package:docdroid/routes/all_routes.dart';
import 'package:docdroid/ui/const.dart';
import 'package:docdroid/ui/widgets/drawerItem.dart';
import 'package:docdroid/ui/widgets/navbarMenu.dart';
import 'package:docdroid/ui/widgets/principalContent.dart';
import 'package:flutter/material.dart';

class PrincipalMenu extends StatelessWidget {
  AuthController _authController = Get.find<AuthController>();
  GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();

  List<Widget> _cardsOptions(BuildContext context) {
    return [
      InkWell(
        child: PrincipalContent(
          title: 'Find Doctor',
          img: 'assets/images/doctor.svg',
        ),
        onTap: () {
          Navigator.pushNamed(context, 'findDoctor');
        },
      ),
      InkWell(
        child: PrincipalContent(
          title: 'Find Hospital',
          img: 'assets/images/hospital.svg',
        ),
        onTap: () => print("tap hospital"),
      ),
      InkWell(
        child: PrincipalContent(
          title: 'Kits',
          img: 'assets/images/kit.svg',
        ),
        onTap: () => print("tap otra cosa"),
      ),
      InkWell(
        child: PrincipalContent(
          title: 'Appointments',
          img: 'assets/images/citas.svg',
        ),
        onTap: () => print('tap citas'),
      ),
      InkWell(
        child: PrincipalContent(
          title: 'Emergency',
          img: 'assets/images/emergency.svg',
        ),
        onTap: () => print("tap emergencias"),
      ),
      InkWell(
        child: PrincipalContent(
          title: 'Drugs Store',
          img: 'assets/images/medicine.svg',
        ),
        onTap: () => print("tap medicina"),
      )
    ];
  }

  Widget _headerDrawer() {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
        colors: AppStyles.colors,
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
      )),
      child: DrawerHeader(
          child: CircleAvatar(
        child: Container(
          decoration: BoxDecoration(
              image:
                  DecorationImage(image: AssetImage('assets/images/user.png'))),
        ),
      )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _key,
        appBar: NavBarMenu(
            title: 'Docdroid',
            icon: Icons.menu,
            onPress: () => _key.currentState.openDrawer()),
        drawer: Drawer(
          child: Container(
            //para poder cambiar el fondo de todo el drawer
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: AppStyles.colors,
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight)),
            child: GetBuilder<AuthController>(
              init: AuthController(),
              builder: (AuthController auth) => ListView(
                children: <Widget>[
                  _headerDrawer(),
                  DrawerItem(title: 'Home', ruta: 'menu'),
                  Divider(),
                  DrawerItem(title: 'Drug', ruta: 'menu'),
                  Divider(),
                  DrawerItem(title: 'Service', ruta: 'menu'),
                  Divider(),
                  DrawerItem(title: 'Dashboard', ruta: 'dashboard'),
                  Divider(),
                  DrawerItem(title: 'Profile', ruta: 'menu'),
                  Divider(),
                  // DrawerItem(title: 'Log out', ruta: 'menu')
                  ListTile(
                    title: Text('log out'.toUpperCase(),
                        style: AppStyles.drawerItem),
                    onTap: () async {
                      auth.logOut();
                    },
                  )
                ],
              ),
            ),
          ),
        ),
        body: SafeArea(
            child: GridView.count(
          crossAxisCount: 2,
          children: _cardsOptions(context),
        )));
  }
}
