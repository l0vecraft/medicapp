import 'package:docdroid/ui/const.dart';
import 'package:docdroid/ui/widgets/customBotton.dart';
import 'package:docdroid/ui/widgets/navbarMenu.dart';
import 'package:flutter/material.dart';

class ForgotPasswordView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          NavBarMenu(
            icon: Icons.arrow_back_ios,
            title: 'Forgot Password',
            onPress: () => Navigator.pop(context),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: Text('We just need your register email to reset',
                style: TextStyle(fontSize: 18, color: Colors.grey[500])),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 18.0),
            child: TextField(
              decoration: InputDecoration(
                  hintText: 'Email',
                  hintStyle: TextStyle(fontSize: 18, color: Colors.grey[500]),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey[500]),
                      borderRadius: BorderRadius.all(Radius.circular(10)))),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30.0),
            child: CustomButton(
              title: 'Reset Password',
              onPress: () => Navigator.pop(context),
            ),
          )
        ],
      ),
    );
  }
}
