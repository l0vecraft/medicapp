import 'package:flutter/material.dart';

class AppStyles {
  //siempre tiene que ir definido static(si se va a cambiar el dato) y el const
  static const List<Color> colors = [Color(0xff029aa8), Color(0xff03ffbc)];

  static final TextStyle buttonText = TextStyle(
      color: Colors.white,
      fontSize: 20.0,
      fontFamily: 'Poppin',
      letterSpacing: 1.2);

  static final TextStyle navTitle =
      TextStyle(fontSize: 30, color: Colors.white, fontFamily: 'Poppin');

  static final TextStyle titleStyle =
      TextStyle(fontSize: 40, color: Colors.lightBlue, fontFamily: 'Poppin');
  static final TextStyle fieldsStyle =
      TextStyle(color: Colors.grey, fontFamily: 'Popping');
  static final TextStyle subtitle =
      TextStyle(fontSize: 16, color: Colors.black, fontFamily: 'Poppin');
  static final TextStyle drawerItem =
      TextStyle(fontSize: 16, color: Colors.white, fontFamily: 'Poppin');
  static final TextStyle modalStyle =
      TextStyle(fontSize: 13, color: Colors.grey[400], fontFamily: 'Poppin');
  static final TextStyle titleModal =
      TextStyle(fontSize: 23, color: Colors.cyan, fontFamily: 'Poppin');
  static final TextStyle subTitleModal =
      TextStyle(fontSize: 15, color: Colors.grey, fontFamily: 'Poppin');
}
